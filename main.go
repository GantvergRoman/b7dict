package main

import (
	. "./mapreducer"
	"./wordreader"
	"./skiptags"
	"io"
	"strconv"
	"strings"
)

var Map = func(stream DataReader, writer MapResultsWriter) (cnt int, err error) {

	var value = MapResultValue("1")

	wordReader := wordreader.NewReader(skiptags.NewReader(stream))
	//wordReader := wordreader.NewReader(stream)

	cnt = 0

mainLoop:
	for {
		key, err := wordReader.Read()
		if err == io.EOF {
			break mainLoop
		}
		if err != nil {
			break mainLoop
		}
		var mapResultKey MapResultKey

		mapResultKey = MapResultKey(strings.ToLower(key))

		err = writer.Write(MapResult{Key: &mapResultKey, Value: &value})
		if err != nil {
			break mainLoop
		}
		cnt++
	}
	return cnt, err
}

var Reduce = func(key *MapResultKey, stream MapResultsReader, writer ResultsWriter) (err error) {
	var sum int

mainLoop:
	for {
		v, e := stream.Read()

		if e == io.EOF {
			break mainLoop
		}

		if e != nil {
			return e
		}

		i, _ := strconv.Atoi(string(*v))
		sum += i
	}

	value := ResultValue(strconv.Itoa(sum))
	mapKey := ResultKey(*key)
	result := Result{Key: &mapKey, Value: &value}

	err = writer.Write(result)
	writer.Flush()

	return err
}

func main() {
	Run(Map, Reduce)
}
