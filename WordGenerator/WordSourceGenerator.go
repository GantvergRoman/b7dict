package main

import (
	"time"
	"math/rand"
	"fmt"
	"os"
	"bufio"
)

const (
	minWordLength        = 1
	maxWordLength        = 11
	maxDictLength        = 1000 * 1000
	numberOfWordsPerFile = (200 * 1024 * 1024 / (2 + minWordLength + (maxWordLength-minWordLength)>>1))
	numberOfVolumes      = 50
)

var (
	punctuation = [...]string{". ", ",", " ", " ", ": ", " ", " ", " ", " ", " ", " ", " ", ";", ":", "\n"}
	alphabet    = "abcDeFgHPoqwz"
)

var (
	words  [maxDictLength]string
	counts [maxDictLength]int
)

func randomWord() string {

	var result string

	wordLength := minWordLength + rand.Intn(maxWordLength)
	for i := 0; i < wordLength; i++ {
		p := rand.Intn(len(alphabet))
		symbol := alphabet[p:p+1]
		result += string(symbol)
	}
	return result
}

func dictGenerator() {
	for i := range words {
		words[i] = randomWord()
		//fmt.Println(words[i])
	}
}

func dictStatSaver(fileName string, words *[]string, counts *[]int) {

	f, _ := os.Create(fmt.Sprintf(fileName))

	writer := bufio.NewWriter(f)
	//defer f.Close()

	for i, w := range *words {
		//writer.WriteString(fmt.Sprintf("%s\t%d\n",w,counts[i]))
		writer.WriteString(fmt.Sprintf("%s\t%d\n", w, (*counts)[i]))
	}
	writer.Flush()
	f.Close()
}

func main() {
	rand.Seed(time.Now().UnixNano())

	dictGenerator()
	ws := words[:]

	for n := 0; n < numberOfVolumes; n++ {
		countsVolume := make([]int, maxDictLength)

		fmt.Println("create volume", n)
		f, _ := os.Create(fmt.Sprintf("test4_%d.txt", n))
		//dict,_:=os.Create(fmt.Sprintf("dict4_%d.txt",n))
		w := bufio.NewWriter(f)

		begin := time.Now()

		for i := 0; i < numberOfWordsPerFile; i++ {
			//wnum:=0 //TODO:
			wnum := rand.Intn(maxDictLength)
			pnum := rand.Intn(len(punctuation))

			w.WriteString(words[wnum])
			w.WriteString(punctuation[pnum])

			counts[wnum] += 1
			countsVolume[wnum] += 1
		}

		dictStatSaver(fmt.Sprintf("dict4_%d.txt", n), &ws, &countsVolume)
		fmt.Printf("Time: %f sec\n", time.Since(begin).Seconds())

		w.Flush()
		f.Close()
	}
	cs := counts[:]
	dictStatSaver("dict4_common.txt", &ws, &cs)
}
