package mapreducer

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"time"
)

type SlaveNode struct {
	nodeIsDone      SignalChannel
	mapperIsDone    SignalChannel
	reducerIsDone   SignalChannel
	api             *SlaveNodeAPI
	masterApiClient *NodeClient
	id              NodeId
}

func (s *SlaveNode) Start() (done SignalChannel, err error) {
	logLine("Slave", "Slave node starting...")

	s.nodeIsDone = make(SignalChannel)

	client, err := rpc.DialHTTPPath("tcp", masterNodeAddress, "/master")
	if err != nil {
		close(s.nodeIsDone)
		return s.nodeIsDone, err
	}
	s.masterApiClient = client

	go s.slaveNodeWorker(done)

	go func() {
		logLine("Slave", "Slave node trying to register...")
		var req RegisterRequest
		var newId NodeId

		req.SlaveRpcAddress = NodeAddress(slaveNodeAddress)

		time.Sleep(time.Duration(delayAfterRegistrationInMs))
		err := (*s.masterApiClient).Call("MasterNodeAPI.Register", req, &newId)
		if err != nil {
			log.Fatal("Registration on master failed.")
		}

		s.id = newId
		logLine("Slave", "slave node API (%s) registered, got slave node id (%v)", slaveNodeAddress, s.id)

	}()
	return s.nodeIsDone, nil
}
func (s *SlaveNode) slaveNodeWorker(done SignalChannel) {

	logLine("Slave", "Slave node API starting...")

	api := new(SlaveNodeAPI)
	api.slaveNode = s
	s.api = api

	slaveServer := rpc.NewServer()
	slaveServer.Register(api)
	slaveServer.HandleHTTP(rpc.DefaultRPCPath, rpc.DefaultDebugPath)
	l, e := net.Listen("tcp", slaveNodeAddress)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	logLine("Slave", "slave node API is listening on %s", slaveNodeAddress)
	http.Serve(l, nil)
}

func (s *SlaveNode) StartMapping() {
	mapper := new(Mapper)
	mapper.node = s
	s.mapperIsDone = mapper.Start()
	s.startMapperIsDoneWatcher()
}

func (s *SlaveNode) StartReducing() {
	reducer := new(Reducer)
	reducer.node = s
	s.reducerIsDone = reducer.Start()
	s.startReducerIsDoneWatcher()
}

func (s *SlaveNode) startMapperIsDoneWatcher() {
	go func() {
		<-s.mapperIsDone
		logLine("Slave (Watcher)", "node %s mapper stopped, report to master..", s.id)
		req := MasterAPIRequest{Id: s.id}
		res := false
		err := (*s.masterApiClient).Call("MasterNodeAPI.ReportMappingIsDone", req, &res)
		if err != nil {
			logLine("Slave (Watcher)", "mapper stoped, but report returned error=", err)
		}
	}()
}

func (s *SlaveNode) startReducerIsDoneWatcher() {
	go func() {
		<-s.reducerIsDone
		logLine("Slave (Watcher)", "node %s reducer stopped, report to master..", s.id)
		req := MasterAPIRequest{Id: s.id}
		res := false
		err := (*s.masterApiClient).Call("MasterNodeAPI.ReportReducingIsDone", req, &res)
		if err != nil {
			logLine("Slave (Watcher)", "reducer stoped, but report returned error=%s", err)
		}
	}()
}

func (s *SlaveNode) Terminate() {
	//TODO: stop RPC server
	close(s.nodeIsDone)
}
