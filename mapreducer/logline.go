package mapreducer

import (
	"fmt"
	"runtime"
	"time"
)

func logLine(moduleName string, formatString string, v ...interface{}) {
	m := new(runtime.MemStats)
	runtime.ReadMemStats(m)
	fmt.Printf("%s [%s]: %s [%s]\n",
		time.Now().Format("15:04:05.000"),
		moduleName,
		fmt.Sprintf(formatString, v...),
		fmt.Sprintf("mem=%dM", m.Sys>>20),
	)
}
