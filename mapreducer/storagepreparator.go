package mapreducer

import "time"

type StoragePreparator interface {
	Init() (err error)
}

type DefaultStoragePreparator struct {
	dbAdapter DbAdapter
}

const (
	mapResultsDropTable   = "DROP TABLE IF EXISTS mapresults"
	resultsDropTable      = "DROP TABLE IF EXISTS results"
	mapResultsCreateTable = `CREATE TABLE mapresults (
                   eventdate Date DEFAULT toDate(now()),
                   key String,
                   value String,
                   hash UInt64 DEFAULT sipHash64(key)
                   ) ENGINE = MergeTree(eventdate, key, 8192)`
	resultsCreateTable = `CREATE TABLE results (
                   eventdate Date DEFAULT toDate(now()),
                   key String,
                   value String
		           ) ENGINE = MergeTree(eventdate, key, 8192)`
)

func (sp *DefaultStoragePreparator) Init() (err error) {
	sp.dbAdapter = new(DefaultDbAdapter)
	sp.dbAdapter.Init()
	var query Query

	err = nil

	query = Query(mapResultsDropTable)

	if e := sp.dbAdapter.Exec(&query); err != nil {
		err = e
	}

	query = Query(resultsDropTable)
	if e := sp.dbAdapter.Exec(&query); err != nil {
		err = e
	}

	query = Query(mapResultsCreateTable)
	if e := sp.dbAdapter.Exec(&query); err != nil {
		err = e
	}

	query = Query(resultsCreateTable)
	if e := sp.dbAdapter.Exec(&query); err != nil {
		err = e
	}

	if err==nil {
		//TODO: fix it later
		time.Sleep(time.Duration(15)*time.Second)
	}

	return err
}
