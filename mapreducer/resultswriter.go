package mapreducer

import "sync"

const reducedResultsInsertTemplate = "INSERT INTO results (key, value) VALUES (?,?)"

type ResultsWriter interface {
	Write(result Result) (err error)
	Init()
	Flush()
}

type DefaultResultsWriter struct {
	results   DbBatch
	dbAdapter DbAdapter
	mx        *sync.Mutex
}

func (rw *DefaultResultsWriter) Write(result Result) (err error) {

	rw.lock()
	defer rw.unlock()

	dbRow := DbRow{string(*result.Key), string(*result.Value)}
	rw.results = append(rw.results, dbRow)

	if len(rw.results) >= insertBatchSize {
		rw.writeResults()
		rw.clearBuffer()
	}

	return
}

func (rw *DefaultResultsWriter) Init() {
	rw.mx = new(sync.Mutex)

	rw.lock()
	defer rw.unlock()

	rw.results = make(DbBatch, 0, insertBatchSize)
	rw.dbAdapter = NewDbAdapter()
	rw.dbAdapter.Init()
}

func (rw *DefaultResultsWriter) Flush() {

	rw.lock()
	defer rw.unlock()

	rw.writeResults()
	rw.clearBuffer()
}

func (rw *DefaultResultsWriter) writeResults() (err error) {
	query := Query(reducedResultsInsertTemplate)
	rw.dbAdapter.BeginBatch(&query)
	rw.dbAdapter.AddToBatch(rw.results)
	err = rw.dbAdapter.SendBatch()
	logLine("Results Writer", "sendBatch() done, err=%v", err)
	return err
}

func (rw *DefaultResultsWriter) clearBuffer() {
	rw.results = rw.results[0:0]
}

func (rw *DefaultResultsWriter) lock() {
	rw.mx.Lock()
}

func (rw *DefaultResultsWriter) unlock() {
	rw.mx.Unlock()
}
