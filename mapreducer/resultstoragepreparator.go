package mapreducer

import (
	"fmt"
	"time"
)

type ResultsStoragePreparator interface {
	Init(nodesList *NodesList) (err error)
}

const (
	intermediateKeysInsertTemplate = `INSERT INTO intermediatekeys_%d (key) 
					SELECT DISTINCT key FROM mapresults WHERE (hash %% %d = %d)`
	intermediateKeysCreateTableTemplate = `CREATE TABLE intermediatekeys_%d (
                   eventdate Date DEFAULT toDate(now()),
                   key String
                   ) ENGINE = MergeTree(eventdate, key, 8192)`
	intermediateKeysDropTableTemplate = "DROP TABLE IF EXISTS intermediatekeys_%d"
)

type DefaultResultsStoragePreparator struct {
	nodesList *NodesList
	dbAdapter DbAdapter
}

func (rsp *DefaultResultsStoragePreparator) Init(nodesList *NodesList) (err error) {
	rsp.dbAdapter = new(DefaultDbAdapter)
	rsp.dbAdapter.Init()
	rsp.nodesList = nodesList

	err = nil

	if e := rsp.dropTables(); e != nil {
		err = e
	}

	if e := rsp.createTables(); e != nil {
		err = e
	}

	if e := rsp.fillTables(); e != nil {
		err = e
	}

	if err==nil {
		//TODO: fix it later
		time.Sleep(time.Duration(150)*time.Second)
	}

	return err
}

func (rsp *DefaultResultsStoragePreparator) dropTables() (err error) {
	for _, nextNode := range *rsp.nodesList {
		query := Query(fmt.Sprintf(intermediateKeysDropTableTemplate, int(nextNode.Id)))
		err = rsp.dbAdapter.Exec(&query)
		if err != nil {
			return err
		}
	}
	return nil
}

func (rsp *DefaultResultsStoragePreparator) createTables() (err error) {
	for _, nextNode := range *rsp.nodesList {
		query := Query(fmt.Sprintf(intermediateKeysCreateTableTemplate, int(nextNode.Id)))
		err = rsp.dbAdapter.Exec(&query)
		if err != nil {
			return err
		}
	}
	return nil
}

func (rsp *DefaultResultsStoragePreparator) fillTables() (err error) {
	for i, nextNode := range *rsp.nodesList {
		query := Query(fmt.Sprintf(intermediateKeysInsertTemplate,
			int(nextNode.Id),
			len(*rsp.nodesList),
			i))
		err = rsp.dbAdapter.Exec(&query)
		if err != nil {
			return err
		}
	}
	return nil
}
