package mapreducer

import (
	"net/rpc"

	"fmt"
	"sync"
)

type NodeId int

func (id NodeId) String() string {
	return fmt.Sprintf("#%d", int(id))
}

type NodeAddress string

type NodeClient = rpc.Client

type NodeStatus byte

const (
	NodeStatusIsIdle       = NodeStatus(iota)
	NodeStatusIsMapping    = NodeStatus(iota)
	NodeStatusIsReducing   = NodeStatus(iota)
	NodeStatusIsTerminated = NodeStatus(iota)
)

type Node struct {
	Id      NodeId
	Address NodeAddress
	Status  NodeStatus
	Client  *NodeClient
}

type NodesList []Node

var mx *sync.Mutex
var nextNodeId = 0

func (list *NodesList) AddNode(node *Node) (id NodeId) {
	if mx == nil {
		mx = new(sync.Mutex)
	}

	mx.Lock()
	defer mx.Unlock()

	newId := NodeId(nextNodeId)
	nextNodeId++

	node.Id = newId
	*list = append(*list, *node)

	return newId
}

func (list *NodesList) GetNodeById(id NodeId) (res *Node) {

	for ix, n := range *list {
		if int(n.Id) == int(id) {
			return &((*list)[ix]) //res , nil
		}
	}

	return nil //nil, errors.New("Incorrect node id")

}
