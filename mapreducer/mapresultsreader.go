package mapreducer

import (
	"fmt"
	"io"
	"sync"
)

const mapResultsSelectTemplate = "SELECT value FROM mapresults WHERE key = ? LIMIT %d, %d"

type MapResultsReader interface {
	Read() (mapResultValue *MapResultValue, err error)
	Init(key *MapResultKey)
}

type DefaultMapResultsReader struct {
	mx            *sync.Mutex
	key           MapResultKey
	currentOffset int
	dbAdapter     DbAdapter
	storedValues  MapResultValueList
}

func (mrr *DefaultMapResultsReader) Read() (mapResultValue *MapResultValue, err error) {

	mrr.mx.Lock()
	defer mrr.mx.Unlock()

	if len(mrr.storedValues) == 0 {
		err = mrr.loadBatch()
		if err != nil {
			return nil, err
		}
	}

	if len(mrr.storedValues) == 0 {
		return nil, io.EOF
	}

	mapResultValue = &mrr.storedValues[len(mrr.storedValues)-1]
	mrr.storedValues = mrr.storedValues[0 : len(mrr.storedValues)-1]

	return mapResultValue, nil
}

func (mrr *DefaultMapResultsReader) Init(key *MapResultKey) {

	mrr.mx = new(sync.Mutex)
	mrr.key = *key
	mrr.currentOffset = 0
	mrr.storedValues = make(MapResultValueList, 0, selectBatchSize)

	mrr.dbAdapter = new(DefaultDbAdapter)
	mrr.dbAdapter.Init()
}

func (mrr *DefaultMapResultsReader) loadBatch() (err error) {

	query := Query(fmt.Sprintf(mapResultsSelectTemplate, mrr.currentOffset, selectBatchSize))
	rows, err := mrr.dbAdapter.Query(&query, string(mrr.key))
	if err != nil {
		return err
	}

	for rows.Next() {
		p := new(MapResultValue)
		err = rows.Scan(p)
		if err != nil {
			return err
		}
		mrr.storedValues = append(mrr.storedValues, *p)
	}

	mrr.currentOffset += selectBatchSize

	return nil
}
