package mapreducer

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"time"
)

type masterNodeStates int

const (
	masterNodeStateIsStarted           masterNodeStates = masterNodeStates(iota)
	masterNodeStateIsMapping           masterNodeStates = masterNodeStates(iota)
	masterNodeStateIsMappingExhausted  masterNodeStates = masterNodeStates(iota)
	masterNodeStateIsGrouping          masterNodeStates = masterNodeStates(iota)
	masterNodeStateIsReducing          masterNodeStates = masterNodeStates(iota)
	masterNodeStateIsReducingExhausted masterNodeStates = masterNodeStates(iota)
	masterNodeStateIsTerminating       masterNodeStates = masterNodeStates(iota)
)

type MasterNode struct {
	currentState masterNodeStates
	nodesList    NodesList
	sourceReader SourceReader
	done         SignalChannel
}

func (m *MasterNode) Start() (done SignalChannel, err error) {

	m.currentState = masterNodeStateIsStarted

	m.nodesList = make(NodesList, 0)

	// make source reader
	m.sourceReader = NewSourceReader()
	m.sourceReader.Init(sourcesFileName)

	m.done = make(SignalChannel)
	<-m.startAPIServer()
	m.startPinger()

	return m.done, nil
}

func (m *MasterNode) startPinger() {

	go func() {
		tick := time.Tick(time.Duration(pingIntervalInMs) * time.Millisecond)
		// check all nodes at each tick
		for {
			select {
			case <-tick:
				logLine("Master (Pinger)", "time to ping!")
				logLine("Master (Pinger)", "nodelist=%+v\n", m.nodesList)

				if m.currentState == masterNodeStateIsTerminating {
					logLine("Master (Pinger)", "master node is terminating, stop pinger.")
					return
				}

				for _, node := range m.nodesList {

					pingRequest := SlaveAPIRequest{}
					pingRequest.Id = node.Id

					pingResponse := false

					logLine("Master (Pinger)", "trying to ping %s, req=%+v", node.Id,pingRequest)

					if node.Client != nil {
						err := (*node.Client).Call("SlaveNodeAPI.Ping", pingRequest, &pingResponse)
						logLine("Master (Pinger)", "node %s ping results: %v, %+v", node.Id, pingResponse, err)
						if !(err == nil) {
							logLine("Master (Pinger)", "ping error, terminating...")
							m.terminateAll()
							return
						}
						if !pingResponse {
							//TODO: fix it later
							//logLine("Master (Pinger)", "pinged but unsuccessful (incorrect id?), terminating...")
							//m.terminateAll()
							//return
						}
					}
				}
			}
		}
	}()
}

func (m *MasterNode) switchMasterNodeStateIfNeeded() {

switchAgain:

	logLine("Master", "switchMasterNodeStateIfNeeded() called")

	switch m.currentState {
	case masterNodeStateIsStarted:

		// prepare storage
		logLine("Master", "starting, prepare storage (before mapping)...")
		prep := NewStoragePreparator()
		err:=prep.Init()
		if err!=nil {
			//TODO: handle error
			logLine("Master", "Storage prepare error! err=%+v",err)
		}

		logLine("Master", "switch state from started to mapping")
		m.currentState = masterNodeStateIsMapping
		goto switchAgain

	case masterNodeStateIsMapping:
		logLine("Master", "nodes should be on mapping, nodes list len=%d", len(m.nodesList))
		for _, node := range m.nodesList {
			m.switchNodeToMapping(node.Id)
		}

	case masterNodeStateIsMappingExhausted:

		logLine("Master", "mapping exhausted, check if all nodes are done..")
		done := true
		for _, node := range m.nodesList {
			if node.Status == NodeStatusIsMapping {
				logLine("Master", "node %s is still mapping..", node.Id)
				done = false
			}
		}

		if done {
			logLine("Master", "mapping is done, switch on grouping..")
			m.currentState = masterNodeStateIsGrouping
			goto switchAgain
		}

	case masterNodeStateIsGrouping:
		logLine("Master", "switched to grouping, grouping TODO.. :(")

		// prepare results storage
		logLine("Master", "preparing results storage (key grouping)...")
		prep := NewResultsStoragePreparator()
		err:=prep.Init(&m.nodesList)
		if err!=nil {
			//TODO: handle error
			logLine("Master", "Results storage prepare error! err=%+v",err)
		}

		// grouping is done, switch on reducing mode
		m.currentState = masterNodeStateIsReducing
		goto switchAgain

	case masterNodeStateIsReducing:
		logLine("Master API", "nodes should be on reducing")
		for _, node := range m.nodesList {
			m.switchNodeToReducing(node.Id)
		}

	case masterNodeStateIsReducingExhausted:

		logLine("Master", "mapping exhausted, check if all nodes are done..")
		reducingDone := true
		for _, node := range m.nodesList {
			if node.Status == NodeStatusIsReducing {
				logLine("Master", "node %s is still reducing..", node.Id)
				reducingDone = false
			}
		}

		if reducingDone {
			logLine("Master", "reducing is done, switch on terminating..")
			m.currentState = masterNodeStateIsTerminating
			goto switchAgain
		}

	case masterNodeStateIsTerminating:
		logLine("Master", "All is done, terminating...")
		m.terminateAll()

	}

}

func (m *MasterNode) switchNodeToReducing(id NodeId) {

	var err error

	logLine("Master", "switch node %v to reducing", id)

	node := m.nodesList.GetNodeById(id)

	if !(node == nil || node.Status == NodeStatusIsReducing || node.Status == NodeStatusIsTerminated) {

		switchReq := SlaveAPIRequest{Id: node.Id}
		switchRes := false

		err = (*node.Client).Call("SlaveNodeAPI.SwitchToReducing", switchReq, &switchRes)

		if !(err == nil) {
			logLine("Master", "switch error: %s", err.Error())
			m.terminateAll()
			return
		}
		node.Status = NodeStatusIsReducing
	}
}

func (m *MasterNode) switchNodeToMapping(id NodeId) {

	var err error

	logLine("Master", "switch node %v to mapping", id)

	node := m.nodesList.GetNodeById(id)

	if !(node == nil || node.Status == NodeStatusIsMapping || node.Status == NodeStatusIsTerminated) {

		switchReq := SlaveAPIRequest{Id: node.Id}
		switchRes := false

		err = (*node.Client).Call("SlaveNodeAPI.SwitchToMapping", switchReq, &switchRes)

		if err != nil {
			logLine("Master", "switch node %v error", node.Id)
			//TODO: do it more sophisticated later
			m.terminateAll()
			return
		}
		node.Status = NodeStatusIsMapping
	}
}

func (m *MasterNode) canToAddNewSlaveNode() (isAllowToAdd bool) {
	switch m.currentState {
	case masterNodeStateIsStarted, masterNodeStateIsMapping, masterNodeStateIsMappingExhausted, masterNodeStateIsGrouping:
		isAllowToAdd = true
	case masterNodeStateIsReducing, masterNodeStateIsReducingExhausted, masterNodeStateIsTerminating:
		isAllowToAdd = false
	default:
		//TODO: add graceful termination
		log.Fatal("Master", "Unknown masterNode state")
	}
	return isAllowToAdd
}

func (m *MasterNode) haveNodesOnMapping() bool {
	for _, node := range m.nodesList {
		if node.Status == NodeStatusIsMapping {
			return true
		}
	}
	return false
}

func (m *MasterNode) haveNodesOnReducing() bool {
	for _, node := range m.nodesList {
		if node.Status == NodeStatusIsReducing {
			return true
		}
	}
	return false
}

func (m *MasterNode) terminateAll() {
	var terminateRequest SlaveAPIRequest
	var terminateResponse bool

	logLine("Master", "terminateAll()")

	for _, node := range m.nodesList {
		if node.Client != nil {
			logLine("Master", "terminate node %s", node.Id)
			terminateResponse = false
			err := (*node.Client).Call("SlaveNodeAPI.Terminate", terminateRequest, &terminateResponse)
			logLine("Master", "slave err=%v, answer=%v", err, terminateResponse)
			node.Status = NodeStatusIsTerminated
		}
	}

	select {
	case <-m.done:
		//already closed
	default:
		close(m.done)
	}
}

func (m *MasterNode) startAPIServer() (started SignalChannel) {
	node := new(MasterNodeAPI)
	node.masterNode = m

	masterServer := rpc.NewServer()

	masterServer.Register(node)
	masterServer.HandleHTTP("/master", "/master-debug")
	listener, e := net.Listen("tcp", masterNodeAddress)
	if e != nil {
		//TODO: replace it
		log.Fatal("listen error:", e)
	}
	go http.Serve(listener, nil)

	logLine("Master", "server is listening on %s", masterNodeAddress)

	ch := make(SignalChannel)

	go func() {
		time.Sleep(time.Duration(delayAfterRegistrationInMs) * time.Millisecond)
		close(ch)
	}()

	return ch
}
