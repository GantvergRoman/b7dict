package mapreducer

import "time"

type Mapper struct {
	node         *SlaveNode
	sourcesBatch SourcesList
}

func (m *Mapper) Start() (isDone SignalChannel) {
	isDone = make(SignalChannel)
	logLine("Mapper", "Start Mapper thread...")
	go m.mappingWorker(isDone)
	return isDone
}

func (m *Mapper) getNextSource() (source *Source) {
	logLine("Mapper", "getNextSource() called")

	if len(m.sourcesBatch) < numberOfThreads {
		logLine("Mapper", "loadNextSourcesBatch() failed")
		if !m.loadNextSourcesBatch() {
			logLine("Mapper", "loadNextSourcesBatch() failed")
		}
	}

	if len(m.sourcesBatch) > 0 {
		source = new(Source)
		*source = m.sourcesBatch[len(m.sourcesBatch)-1]
		m.sourcesBatch = m.sourcesBatch[0 : len(m.sourcesBatch)-1]
	}

	return source
}

func (m *Mapper) mappingWorker(isDone SignalChannel) {

	logLine("Mapper", "mappingWorker is started")

	//unique id of map task
	var taskId int
	// set of wrapper channels
	mappingChannels := make(map[int]SignalChannel)
	// channel of closed wrapper channels
	closedWrapperChannels := make(chan SignalChannel)

mapperLoop:
	for {

		logLine("Mapper", "Check finished workers...")
		//exclude closed channels from the list
		for i, c := range mappingChannels {
			//check if closed
			select {
			case <-c:
				logLine("Mapper", "Exclude wrapper[%d] that done from the list...", i)
				//exclude closed channel from the list
				delete(mappingChannels, i)
			default:
			}
		}

		if len(mappingChannels) < numberOfThreads {

			//get new source
			source := m.getNextSource()
			logLine("Mapper", "got next source %v", source)
			logLine("Mapper", "count of workers=%d", len(mappingChannels))

			//if no new sources and no working wrappers, break main loop
			if source == nil && len(mappingChannels) == 0 {
				logLine("Mapper",
					"no sources, no open channels - break")
				break mapperLoop
			}

			// no new source
			if source == nil {
				//TODO: remove sleep()
				time.Sleep(time.Duration(1) * time.Second)
				continue mapperLoop
			}

			// set new unique map task id
			taskId++

			//create new map wrapper
			logLine("Mapper", "Creating new map wrapper #%d for (%+v)", taskId, source.Key)
			wrapperIsDone := m.mapWrapper(source)
			//save channel of new process
			mappingChannels[taskId] = wrapperIsDone

			//start task watcher
			go func(ch SignalChannel) {
				//wait until the wrapper is done
				logLine("Mapper (watcher)", "channel watcher set...")
				<-ch
				logLine("Mapper (watcher)", "channel is closed, report...")
				//send closed channel into channel
				closedWrapperChannels <- ch
			}(wrapperIsDone)

		}

		//if maximum threads count reached
		//waiting until one of wrappers is done
		if len(mappingChannels) >= numberOfThreads {
			logLine("Mapper", "No slots, waiting for closed wrapper...")
			<-closedWrapperChannels
		}
	}

	logLine("Mapper", "All done, send mapper done signal and close mapper...")
	close(isDone)
}

func (m *Mapper) mapWrapper(source *Source) (ch SignalChannel) {
	logLine("Mapper (Wrapper)", "mapWrapper() for '%s' started...", source.Key)
	ch = make(SignalChannel)
	go func(ch SignalChannel) {
		reader := NewDataReader()
		writer := NewMapResultsWriter()
		reader.Init(source)
		writer.Init()
		cnt, err:=mapFunction(reader, writer)
		logLine("Mapper (Wrapper)", "Map() done, count=%d, err=%v",cnt, err)
		writer.Flush()
		logLine("Mapper (Wrapper)", "mapWrapper() for '%s' finished.", source.Key)
		close(ch)
	}(ch)
	return ch
}

func (m *Mapper) loadNextSourcesBatch() (ok bool) {

	req := MasterAPIRequest{Id: m.node.id}
	sourcesList := SourcesList{}

	err := (*m.node.masterApiClient).Call("MasterNodeAPI.GetSourcesList", req, &sourcesList)
	logLine("Mapper", "getSourcesList call results: %v, %+v", sourcesList, err)

	if err != nil {
		//error, nothing to do
		return false
	}
	if len(sourcesList) == 0 {
		//no new sources, nothing to do
		return true
	}

	logLine("Mapper",
		"loadNextSourcesBatch(): add %d sources to the current batch",
		len(sourcesList))

	m.sourcesBatch = append(m.sourcesBatch, sourcesList...)

	logLine("Mapper",
		"loadNextSourcesBatch(): current batch=%+v",
		m.sourcesBatch)

	return true
}
