package mapreducer

type ReduceFunc func(key *MapResultKey, stream MapResultsReader, writer ResultsWriter) (err error)

type MapFunc func(stream DataReader, writer MapResultsWriter) (count int, err error)

type SourceKey string

type Source struct {
	Key SourceKey
}

type SourcesList []Source

type Data struct {
	Reader *SourceReader
}

type ResultKey string

type ResultValue string

type Result struct {
	Key   *ResultKey
	Value *ResultValue
}

type MapResultKey string
type MapResultKeyList = []MapResultKey

type MapResultValue string
type MapResultValueList = []MapResultValue

type MapResult struct {
	Key   *MapResultKey
	Value *MapResultValue
}

type DestinationKey string

type Destination struct {
	Key *DestinationKey
}

type SignalChannel chan struct{}

type RegisterRequest struct {
	SlaveRpcAddress NodeAddress
}
type MasterAPIRequest struct {
	Id NodeId
}
type SlaveAPIRequest struct {
	Id NodeId
}

type DbRow = []interface{}
type DbBatch []DbRow

//TODO: Maybe theses vars have to be deleted
// types for job with database
type TableName string
type RecordBlockSize int
type Query string
