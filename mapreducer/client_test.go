package mapreducer

import (
	"fmt"
	"log"
	"net/rpc"
)

func test() {
	client, err := rpc.DialHTTP("tcp", "0.0.0.0"+":9999")
	if err != nil {
		log.Fatal("dialing:", err)
	}

	ipport := NodeAddress("192.168.1.0:8082")

	req1 := &RegisterRequest{&ipport}
	var reply1 int
	err = client.Call("masterNodeAPI.Register", req1, &reply1)
	if err != nil {
		log.Fatal("registered error:", err)
	}
	Id := NodeId(reply1)
	fmt.Print(Id)

	req2 := &MasterAPIRequest{Id}
	var reply2 string
	err = client.Call("masterNodeAPI.GetSourcesList", req2, &reply2)
	if err != nil {
		log.Fatal("registered error:", err)
	}
	fmt.Print(reply2)
	err = client.Call("masterNodeAPI.GetSourcesList", req2, &reply2)
	if err != nil {
		log.Fatal("registered error:", err)
	}
	fmt.Print(reply2)
}
