package mapreducer

type SlaveNodeAPI struct {
	slaveNode *SlaveNode
}

func (api *SlaveNodeAPI) Ping(req *SlaveAPIRequest, reply *bool) (err error) {
	logLine("Slave API", "Ping(%v) called", api.slaveNode.id)
	//TODO: fix it later
	*reply=true
	//*reply = api.slaveNode.id == req.Id
	return nil
}

func (api *SlaveNodeAPI) SwitchToMapping(req *SlaveAPIRequest, reply *bool) (err error) {
	logLine("Slave API", "SwitchToMapping(%v) called", api.slaveNode.id)
	api.slaveNode.StartMapping()
	return nil
}

func (api *SlaveNodeAPI) SwitchToReducing(req *SlaveAPIRequest, reply *bool) (err error) {
	logLine("Slave API", "SwitchToReducing(%v) called", api.slaveNode.id)
	api.slaveNode.StartReducing()
	return nil
}

func (api *SlaveNodeAPI) Terminate(req *SlaveAPIRequest, reply *bool) (err error) {
	logLine("Slave API", "Terminate(%v) called", api.slaveNode.id)
	api.slaveNode.Terminate()
	*reply = true
	return nil
}
