package mapreducer

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"os"
)

type MainSectionXMLStructure struct {
	IsMasterNode      bool   `xml:"isMasterNode"`
	SourcesFileName   string `xml:"sourcesFileName"`
	DatabaseDriver    string `xml:"databaseDriver"`
	DatabaseDSN       string `xml:"databaseDSN"`
	MasterNodeAddress string `xml:"masterNodeAddress"`
	SlaveNodeAddress  string `xml:"nodeAddress"`
	NumberOfThreads   int    `xml:"numberOfThreads"`
	SelectBatchSize   int    `xml:"selectBatchSize"`
	InsertBatchSize   int    `xml:"insertBatchSize"`
}

func ReadConfigFile() {
	MainSection := new(MainSectionXMLStructure)
	xmlFile, err := os.Open("config.xml")
	if err != nil {
		log.Fatal(err)
		return
	}
	defer xmlFile.Close()

	xmlByteFile, _ := ioutil.ReadAll(xmlFile)
	xml.Unmarshal(xmlByteFile, &MainSection)
	if err != nil {
		log.Fatal(err)
	}

	isMasterNode = MainSection.IsMasterNode
	sourcesFileName = MainSection.SourcesFileName
	databaseDriver = MainSection.DatabaseDriver
	databaseDSN = MainSection.DatabaseDSN
	masterNodeAddress = MainSection.MasterNodeAddress
	slaveNodeAddress = MainSection.SlaveNodeAddress
	numberOfThreads = MainSection.NumberOfThreads
	selectBatchSize = MainSection.SelectBatchSize
	insertBatchSize = MainSection.InsertBatchSize
	return
}
