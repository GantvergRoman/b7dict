package mapreducer

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/kshvakov/clickhouse"
	"sync"
)

type DbAdapter interface {
	Init() error
	Ping() error
	BeginBatch(query *Query) (err error)
	AddToBatch(rows DbBatch) (err error)
	SendBatch() (err error)
	Query(query *Query, params ...interface{}) (res *sql.Rows, err error)
	Exec(query *Query, params ...interface{}) (err error)
}

var globalDb *sql.DB

type DefaultDbAdapter struct {
	db   *sql.DB
	tx   *sql.Tx
	stmt *sql.Stmt
	mx   *sync.Mutex
}

func (dba *DefaultDbAdapter) Init() (err error) {

	//init value of mx
	if dba.mx == nil {
		dba.mx = new(sync.Mutex)
	}

	err = dba.Ping()
	logLine("DBAdapter Init", "Ping() called, err=%v", err)
	return err
}

func (dba *DefaultDbAdapter) Ping() error {
	dba.lock()
	defer dba.unlock()

	dba.openConn()
	err := dba.db.Ping()
	if err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			return errors.New(fmt.Sprintf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace))
		} else {
			return err
		}
	}
	dba.closeConn()
	return nil
}

func (dba *DefaultDbAdapter) BeginBatch(queryTemplate *Query) (err error) {

	dba.lock()
	defer dba.unlock()

	dba.openConn()
	dba.tx, err = dba.db.Begin()

	if err != nil {
		return err
	}

	dba.stmt, err = dba.tx.Prepare(string(*queryTemplate))

	return err
}

func (dba *DefaultDbAdapter) SendBatch() (err error) {

	dba.lock()
	defer dba.unlock()

	if dba.tx != nil {
		err = dba.tx.Commit()
		dba.tx = nil
	}
	dba.closeConn()

	//logLine("SendBatch", "err=%+v", err)

	return err
}

func (dba *DefaultDbAdapter) AddToBatch(rows DbBatch) (err error) {

	dba.lock()
	defer dba.unlock()

	var e error = nil

	if dba.tx == nil || dba.stmt == nil {
		logLine("AddToBatch", "tx=%+v,stmt=%+v", dba.tx, dba.stmt)
		return errors.New("no active transaction now")
	}

	//	logLine("AddToBatch", "batch len=%d", len(rows))

	for _, row := range rows {
		_, e = dba.stmt.Exec(row...)
	}

	//logLine("AddToBatch", "err=%+v, exit...", e)

	return e
}

func (dba *DefaultDbAdapter) Exec(query *Query, params ...interface{}) (err error) {
	//dba.lock()
	//defer dba.unlock()

	//	logLine("DBAdapter", "Exec('%v'), len(params)=%d", *query, len(params))

	dba.openConn()
	_, err = dba.db.Exec(string(*query), params...)
	dba.closeConn()

	return err
}

func (dba *DefaultDbAdapter) Query(query *Query, params ...interface{}) (res *sql.Rows, err error) {
	//dba.lock()
	//defer dba.unlock()

	//	logLine("DBAdapter", "Query('%v'), len(params)=%d, params=%+v", *query, len(params), params)

	dba.openConn()

	// dba.SendBatch()

	if params == nil {
		res, err = dba.db.Query(string(*query))
	} else {
		res, err = dba.db.Query(string(*query), params...)
	}

	dba.closeConn()

	if err != nil {
		return nil, err
	}
	return res, nil
}

func (dba *DefaultDbAdapter) openConn() (err error) {
	err = nil

	if dba.db == nil {
		if globalDb == nil {
			globalDb, err = sql.Open(databaseDriver, databaseDSN)
		}
		dba.db = globalDb
		logLine("DBAdapter", "openConn() err=%v", err)
	}

	return err
}

func (dba *DefaultDbAdapter) closeConn() {
	//dba.db.Close()
}

func (dba *DefaultDbAdapter) lock() {
	//dba.mx.Lock()
}

func (dba *DefaultDbAdapter) unlock() {
	//dba.mx.Unlock()
}

//CREATE TABLE mapresults (
//eventdate Date DEFAULT toDate(now()),
//key String,
//value String,
//hash UInt64
//) ENGINE = MergeTree(eventdate, key, 8192)

//CREATE TABLE results (
//eventdate Date DEFAULT toDate(now()),
//key String,
//value String
//) ENGINE = MergeTree(eventdate, key, 8192)

//CREATE TABLE intermediatekeys_0 (
//eventdate Date DEFAULT toDate(now()),
//key String
//) ENGINE = MergeTree(eventdate, key, 8192)
