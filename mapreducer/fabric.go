package mapreducer

// funcs return objects of concrete types for readers and writers
func NewSourceReader() SourceReader                     { return new(DefaultSourceReader) }
func NewDataReader() DataReader                         { return new(DefaultDataReader) }
func NewMapResultsWriter() MapResultsWriter             { return new(DefaultMapResultsWriter) }
func NewMapResultsReader() MapResultsReader             { return new(DefaultMapResultsReader) }
func NewResultsWriter() ResultsWriter                   { return new(DefaultResultsWriter) }
func NewIntermediateKeysReader() IntermediateKeysReader { return new(DefaultIntermediateKeysReader) }
func NewStoragePreparator() StoragePreparator           { return new(DefaultStoragePreparator) }
func NewResultsStoragePreparator() ResultsStoragePreparator {
	return new(DefaultResultsStoragePreparator)
}

// use global adapter
func NewDbAdapter() DbAdapter {
	return new(DefaultDbAdapter)
	//return globalDbAdapter
}

func init() {
	globalDbAdapter = new(DefaultDbAdapter)
}
