package mapreducer

import (
	"bufio"
	"log"
	"os"
	"strings"
)

type SourceReader interface {
	Init(sourceName string)
	GetSource() (src *Source, err error)
}

type DefaultSourceReader struct {
	reader *bufio.Reader
}

func (r *DefaultSourceReader) GetSource() (src *Source, err error) {
	s, err := r.reader.ReadString('\n')
	if err != nil {
		return nil, err
	}

	src = new(Source)
	src.Key = SourceKey(strings.TrimSpace(s))

	logLine("SourceReader", "Key '%s' read...", src.Key)

	return src, nil
}

func (r *DefaultSourceReader) Init(listFilename string) {
	logLine("SourceReader", "Trying to open sources list '%s'...", listFilename)
	f, err := os.Open(listFilename)
	if err != nil {
		log.Fatal("Source list open error", err)
	}
	r.reader = bufio.NewReader(f)

}
