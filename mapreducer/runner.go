package mapreducer

func Run(mapping MapFunc, reducing ReduceFunc) {
	mapFunction, reduceFunction = mapping, reducing

	var masterNodeIsDone SignalChannel
	if isMasterNode {
		mNode := new(MasterNode)
		masterNodeIsDone, _ = mNode.Start()
	}

	sNode := new(SlaveNode)
	slaveNodeIsDone, _ := sNode.Start()

	logLine("Runner", "Waiting until slave node process is done...")

	//wait until all is done
	<-slaveNodeIsDone
	if isMasterNode {
		logLine("Runner", "Waiting until master node process is done...")
		<-masterNodeIsDone
	}
}
