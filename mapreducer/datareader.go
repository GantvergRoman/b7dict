package mapreducer

import (
	"bufio"
	"compress/bzip2"
	"io"
	"os"
	"path/filepath"
)

type DataReader interface {
	io.Reader
	Init(source *Source) (err error)
}

type DefaultDataReader struct {
	reader *bufio.Reader
}

func (r *DefaultDataReader) Read(p []byte) (n int, err error) {
	return r.reader.Read(p)
}

func (r *DefaultDataReader) Init(source *Source) (err error) {
	f, err := os.Open(string(source.Key))
	//TODO: error handler
	switch filepath.Ext(string(source.Key)) {
	case ".bz2", ".bzip2":
		logLine("DataReader Init","BZ2: %s",source.Key)
		r.reader = bufio.NewReader(bzip2.NewReader(f))
	default:
		logLine("DataReader Init","TXT: %s",source.Key)
		r.reader = bufio.NewReader(f)
	}
	return err
}
