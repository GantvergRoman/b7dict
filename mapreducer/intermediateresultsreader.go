package mapreducer

import (
	"database/sql"
	"fmt"
	"io"
	"sync"
)

type IntermediateKeysReader interface {
	Read() (mapResultKey *MapResultKey, err error)
	Init(nodeId NodeId)
}

const (
	selectIntermediateQuery = "SELECT DISTINCT key FROM intermediatekeys_%d ORDER BY key LIMIT %d,%d"
)

type DefaultIntermediateKeysReader struct {
	nodeId           NodeId
	dbAdapter        DbAdapter
	currentPosition  int
	mapResultKeyList MapResultKeyList
	mx               *sync.Mutex
}

func (ikr *DefaultIntermediateKeysReader) Read() (mapResultKey *MapResultKey, err error) {

	ikr.mx.Lock()
	defer ikr.mx.Unlock()

	if len(ikr.mapResultKeyList) == 0 {
		var rows *sql.Rows
		rows, err = ikr.readResults()
		if err != nil {
			return nil, err
		}

		for rows.Next() {
			var key string
			err = rows.Scan(&key)
			if err != nil {
				return nil, err
			}

			ikr.mapResultKeyList = append(ikr.mapResultKeyList, MapResultKey(key))
		}
	}

	if len(ikr.mapResultKeyList) == 0 {
		return nil, io.EOF
	}

	mapResultKey = &ikr.mapResultKeyList[len(ikr.mapResultKeyList)-1]
	ikr.mapResultKeyList = ikr.mapResultKeyList[0 : len(ikr.mapResultKeyList)-1]
	return mapResultKey, nil
}

func (ikr *DefaultIntermediateKeysReader) Init(nodeId NodeId) {

	if ikr.mx == nil {
		ikr.mx = new(sync.Mutex)
	}

	ikr.mx.Lock()
	defer ikr.mx.Unlock()

	ikr.nodeId = nodeId
	ikr.dbAdapter = new(DefaultDbAdapter)
	ikr.dbAdapter.Init()

	ikr.currentPosition = 0
	ikr.mapResultKeyList = make(MapResultKeyList, 0, selectBatchSize+1000)
}

func (ikr *DefaultIntermediateKeysReader) readResults() (rows *sql.Rows, err error) {
	query := Query(fmt.Sprintf(selectIntermediateQuery, int(ikr.nodeId), ikr.currentPosition, selectBatchSize))
	logLine("ImResults Reader", "Reading from %d, ofs:len= %d,%d, query=%s", int(ikr.nodeId), ikr.currentPosition, selectBatchSize, string(query))
	rows, err = ikr.dbAdapter.Query(&query)
	logLine("ImResults Reader", "Read, rows=%v, err=%v", rows, err)
	ikr.currentPosition += selectBatchSize
	return rows, err
}
