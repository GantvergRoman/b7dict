package mapreducer

import (
	"math/rand"
	"time"
)

const (
	minLength = 1
	maxLength = 10
	count     = 100000
)

func main() {
	resultsWriter := new(DefaultResultsWriter)

	for i := 0; i < count*1.2; i++ {
		var key = ResultKey(randomWord())
		var value = ResultValue("1")
		var res = Result{
			Key:   &key,
			Value: &value}
		resultsWriter.Write(res)
	}
	resultsWriter.Flush()

	/*ch := make(chan mapreducer.Result)
	chClose := make(chan mapreducer.SignalChannel)
	go clickhouse.Put(ch, chClose,"example")
	for i := 0; i < bufferSize*1.2; i++ {
		entity := mapreducer.Result{Key: mapreducer.ResultKey(randomWord()), Value: "1"}
		ch <- entity
	}
	close(ch)
	for {
		_, opened := <-chClose
		if !opened {
			return
		}
	}
		hashChanel:=make(chan uint64)
		go clickhouse.GetAllHashes(hashChanel)
		for{
			_,opened:=<-hashChanel
			if !opened{
				return
			}
		}
	chDto := make(chan common.DTO)
	go clickhouse.GetHashesBy(chDto, 16190893787798805688)
	for {
		dto, opened := <-chDto
		if !opened {
			break
		}
		fmt.Println(dto)
	}*/
}

func randomWord() string {
	var result string
	rand.Seed(time.Now().UnixNano())
	count := minLength + rand.Intn(maxLength)
	for i := 0; i < count; i++ {
		c := 'a' + rand.Intn(26)
		result += string(c)
	}
	return result
}
