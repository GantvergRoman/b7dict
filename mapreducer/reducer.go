package mapreducer

import (
	"time"
)

type Reducer struct {
	node       *SlaveNode
	keysReader IntermediateKeysReader
	writer     ResultsWriter
}

func (r *Reducer) Start() (isDone SignalChannel) {

	isDone = make(SignalChannel)
	//TODO:
	//prepareDatabaseForReducing()
	r.keysReader = NewIntermediateKeysReader()
	r.keysReader.Init(r.node.id)
	r.writer = NewResultsWriter()
	r.writer.Init()
	go r.reducingWorker(isDone)
	return isDone
}

func (r *Reducer) reducingWorker(isDone SignalChannel) {
	logLine("Reducer", "reducingWorker is started")

	//unique id of reduce task
	var reduceTaskId int
	// set of wrapper channels
	//reducingChannels := make(map[int]SignalChannel)
	// channel of closed wrapper channels
	closedWrapperChannels := make(chan int)

	//TODO: rewrite this shit
	tick5sec := time.Tick(time.Duration(5) * time.Second)

	threadsCount := 0

reducerLoop:
	for {

		logLine("Reducer", "Check finished workers...")

		//check if closed
		select {
		case id := <-closedWrapperChannels:
			logLine("Reducer", "Exclude wrapper[%d] that done from the list...", id)
			//exclude closed channel from the list
			//delete(reducingChannels, id)
			threadsCount--
		case <-tick5sec:
			//if maximum threads count reached
			//waiting until one of wrappers is done

			//TODO: rewrite this shit
			if threadsCount >= numberOfThreads {
				logLine("Reducer", "No slots, waiting for closed wrapper...")
			}
		}

		if threadsCount < numberOfThreads {

			logLine("Reducer", "count of reduce workers=%d", threadsCount)
			//get new key
			logLine("Reducer", "trying to get next key....")
			nextKey := r.getNextKey()

			//if no new keys and no working wrappers, break main loop
			if nextKey == nil && threadsCount == 0 {
				logLine("Reducer",
					"no keys, no open channels - break")
				break reducerLoop
			}

			// no new key
			if nextKey == nil {
				logLine("Reducer", "next key is nil, sleep for a time")
				//TODO: remove sleep()
				time.Sleep(time.Duration(10) * time.Second)
				continue reducerLoop
			}
			logLine("Reducer", "got next key %#v", *nextKey)

			//create new reduce wrapper
			reduceTaskId++ // set new unique reduce task id
			logLine("Reducer", "Creating new wrapper #%d", reduceTaskId)
			wrapperIsDone := r.reduceWrapper(nextKey, r.writer)
			//save channel of new process
			threadsCount++

			//start task watcher
			go func(ch SignalChannel, id int) {
				//wait until the wrapper is done
				logLine("Reducer (watcher)", "channel watcher set [%d]...", id)
				<-ch
				logLine("Reducer (watcher)", "channel [%d] is closed, report...", id)
				//send closed channel into channel
				closedWrapperChannels <- id
			}(wrapperIsDone, reduceTaskId)

		}

	}

	r.writer.Flush()

	logLine("Reducer", "All done, send reducer done signal and exit...")

	close(isDone)
}

func (r *Reducer) reduceWrapper(key *MapResultKey, commonWriter ResultsWriter) (ch SignalChannel) {
	logLine("Reducer (Wrapper)", "reduceWrapper() for '%s' started...", *key)
	ch = make(SignalChannel)
	go func(ch SignalChannel) {
		reader := NewMapResultsReader()
		reader.Init(key)
		//writer := NewResultsWriter()
		//writer.Init()
		writer := commonWriter
		err := reduceFunction(key, reader, writer)
		logLine("Reducer (Wrapper)", "Reduce('%s') done, err=%v.", *key, err)
		logLine("Reducer (Wrapper)", "reduceWrapper() for '%s' finished.", *key)

		close(ch)
	}(ch)
	return ch
}

func (r *Reducer) getNextKey() (key *MapResultKey) {
	key, _ = r.keysReader.Read()
	return key
}
