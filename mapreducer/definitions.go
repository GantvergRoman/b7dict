package mapreducer

var (
	mapFunction     MapFunc
	reduceFunction  ReduceFunc
	globalDbAdapter DbAdapter
)

// Configuration values
var (
	isMasterNode      bool
	sourcesFileName   string
	databaseDriver    string
	databaseDSN       string
	masterNodeAddress string
	slaveNodeAddress  string
	numberOfThreads   int
	selectBatchSize   int
	insertBatchSize   int
)

var (
	sources *SourcesList
)
