package mapreducer

import (
	"errors"
	"io"
	"log"
	"net/rpc"
)

const (
	delayAfterRegistrationInMs = 1500
	pingIntervalInMs           = 15000
)

type MasterNodeAPI struct {
	masterNode *MasterNode
}

func (api *MasterNodeAPI) Register(req *RegisterRequest, reply *NodeId) (err error) {
	logLine("Master API", "Register() called")

	client, err := rpc.DialHTTP("tcp", string(req.SlaveRpcAddress))
	if err != nil {
		logLine("Master API", "rpc.DialHTTP error:", err.Error())
		return err
	}
	if !api.masterNode.canToAddNewSlaveNode() {
		logLine("Master API", "Registration is forbidden")
		return errors.New("Registration is forbidden")
	}

	// register a new node (into the list inside of masterNode)
	node := new(Node)
	node.Client = client
	node.Address = NodeAddress(req.SlaveRpcAddress)
	node.Status = NodeStatusIsIdle
	logLine("Master API", "New node=%+v\n", *node)
	id := api.masterNode.nodesList.AddNode(node)
	logLine("Master API", "New node %v is adding\n", int(id))
	node = api.masterNode.nodesList.GetNodeById(id)
	if node == nil {
		//TODO: fix
		log.Fatal("can't find node by id")
	}
	*reply = id
	logLine("Master API", "node added=%+v,id=%+v, *reply=%+v\n", *node, id, *reply)
	logLine("Master API", "nodelist=%+v\n", api.masterNode.nodesList)

	// ask master to switch current state if needed
	api.masterNode.switchMasterNodeStateIfNeeded()

	return nil
}

func (api *MasterNodeAPI) GetSourcesList(req *MasterAPIRequest, reply *SourcesList) (err error) {
	logLine("Master API", "GetSourcesList() called")

	var list SourcesList

	logLine("Master API", "Read from source reader...")
	source, err := api.masterNode.sourceReader.GetSource()
	logLine("Master API", "err=%v,list=%v", err, source)

	if err == io.EOF {
		//no more sources, it's ok
		return nil
	}
	if err != nil {
		return err
	}

	//return one source as list
	list = SourcesList{*source}
	*reply = list

	logLine("Master API", "List of %d sources will be returned", len(list))
	return nil
}

func (api *MasterNodeAPI) ReportMappingIsDone(req *MasterAPIRequest, reply *bool) (err error) {
	logLine("Master API", "ReportMappingIsDone(%+v) called", req.Id)
	node := api.masterNode.nodesList.GetNodeById(req.Id)
	node.Status = NodeStatusIsIdle
	*reply = true

	api.masterNode.currentState = masterNodeStateIsMappingExhausted
	api.masterNode.switchMasterNodeStateIfNeeded()

	return nil
}

func (api *MasterNodeAPI) ReportReducingIsDone(req *MasterAPIRequest, reply *bool) (err error) {

	logLine("Master API", "ReportReducingIsDone(%+v) called", req.Id)

	node := api.masterNode.nodesList.GetNodeById(req.Id)
	node.Status = NodeStatusIsIdle
	*reply = true

	api.masterNode.currentState = masterNodeStateIsReducingExhausted
	api.masterNode.switchMasterNodeStateIfNeeded()

	return nil
}
