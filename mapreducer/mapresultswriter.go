package mapreducer

import "sync"

const (
	mapResultsInsertTemplate = "INSERT INTO mapresults (key, value) VALUES (?,?)"
)

type MapResultsWriter interface {
	Write(mapResult MapResult) (err error)
	Init()
	Flush()
}

type DefaultMapResultsWriter struct {
	results   DbBatch
	dbAdapter DbAdapter
	mx        *sync.Mutex
}

func (mrw *DefaultMapResultsWriter) Write(mapResult MapResult) (err error) {

	mrw.mx.Lock()
	defer mrw.mx.Unlock()

	err = nil

	mrw.results = append(mrw.results, DbRow{string(*mapResult.Key), string(*mapResult.Value)})
	if len(mrw.results) >= insertBatchSize {
		err = mrw.writeResults()
		mrw.results = mrw.results[0:0]
	}
	return err
}

func (mrw *DefaultMapResultsWriter) Init() {

	if mrw.mx == nil {
		mrw.mx = new(sync.Mutex)
	}

	mrw.mx.Lock()
	defer mrw.mx.Unlock()

	mrw.results = make(DbBatch, 0, insertBatchSize)
	mrw.dbAdapter = new(DefaultDbAdapter)
	mrw.dbAdapter.Init()
}

func (mrw *DefaultMapResultsWriter) Flush() {

	mrw.mx.Lock()
	defer mrw.mx.Unlock()

	mrw.writeResults()
	mrw.results = mrw.results[0:0]
}

func (mrw *DefaultMapResultsWriter) writeResults() (err error) {

	query := Query(mapResultsInsertTemplate)

	err = mrw.dbAdapter.BeginBatch(&query)

	if err != nil {
		return err
	}

	err = mrw.dbAdapter.AddToBatch(mrw.results)

	if err != nil {
		return err
	}

	err = mrw.dbAdapter.SendBatch()

	if err != nil {
		return err
	}

	return err
}
