package main

import (
	"./wordreader"
	"compress/bzip2"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

//Path to compressed text file
const FILENAME = "wordreader/testdata/wiki_60.bz2"

//If true: All read words will be shown in console, in that case processing time will NOT be measured properly
const CONSOLE_OUTPUT = true

func main() {
	f, err := os.Open(FILENAME)
	if err != nil {
		log.Fatal("Unable to open file:", err)
	}
	defer f.Close()
	wReader := wordreader.NewReader(bzip2.NewReader(f))
	fmt.Println("Starting to read file word by word")
	startTime := time.Now()
	for {
		word, err := wReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal("Error while reading file:", err)
		}
		if CONSOLE_OUTPUT {
			fmt.Println(word)
		}
	}
	dur := time.Since(startTime)
	stat, _ := f.Stat()
	stat.Size()
	fmt.Printf("Word by word reading is finished\nTotal time: %v, Av. Speed: %.2f MB/s\n", dur.Seconds(), float64(stat.Size())/(1024*1024)/dur.Seconds())
}
