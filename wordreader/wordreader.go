package wordreader

import (
	"bufio"
	"io"
	"unicode"
	"unicode/utf8"
)

type WordReader struct {
	bufR *bufio.Reader
}

//Returns new word reader
func NewReader(dataReader io.Reader) (res *WordReader) {
	res = new(WordReader)
	res.bufR = bufio.NewReader(dataReader)
	return res
}

//Reads next word
func (w *WordReader) Read() (word string, err error) {
	var currR rune

	wordBytes := make([]byte, 0, 512)
	buf := make([]byte, 4, 4)

	for {
		currR, _, err = w.bufR.ReadRune()
		if err != nil {
			break
		}

		if unicode.IsLetter(currR) {
			n := utf8.EncodeRune(buf, currR)
			wordBytes = append(wordBytes, buf[0:n]...)
		} else {
			if len(wordBytes) > 0 {
				break
			}
		}
	}

	if len(wordBytes) > 0 {
		err = nil
	}

	return string(wordBytes), err
}
