package skiptags

import (
	"bufio"
	"io"
	"unicode/utf8"
)

const (
	runeMaxSize     = 4
	bufferSize      = 1024
	bufferAllocSize = bufferSize + (runeMaxSize - 1)
)

type SkipTagsReader struct {
	reader        *bufio.Reader
	skipNow       bool
	buf           []byte
	multilineTags bool // if false, new line closes tag
}

//Returns new skip-tags reader
func NewReader(dataReader io.Reader) (res *SkipTagsReader) {

	res = new(SkipTagsReader)

	res.multilineTags = false
	res.reader = bufio.NewReader(dataReader)
	res.buf = make([]byte, 0, bufferAllocSize)

	return res
}

//Reads next data
func (r *SkipTagsReader) Read(p []byte) (n int, err error) {

	if len(p) == 0 {
		return 0, nil
	}

	err = r.loadBuf()
	n = copy(p, r.buf)
	////println("Read(), len(p)=", len(p), "len(r.buf)=", len(r.buf))
	////println("Read(), copied, bytes count=", n, "err=", err)

	//assert: n == min[len(p), len(r.buf)]
	if n == len(r.buf) {
		r.buf = r.buf[0:0]
	} else {
		// assert: n==len(p) && len(p) < len(r.buf)
		buf := make([]byte, len(r.buf)-n, bufferAllocSize)
		copy(buf, r.buf[n:])
		r.buf = buf
	}

	return n, err
}

func (r *SkipTagsReader) loadBuf() (err error) {

	bytes := make([]byte, 4, 4)

	for len(r.buf) < bufferSize {
		curR, curSize, e := r.reader.ReadRune()
		////print("["+string(curR), err, "]")
		if curSize > 0 {
			switch curR {
			case '<':
				r.skipNow = true
			case '>':
				r.skipNow = false
			case '\n', '\r':
				if !r.multilineTags {
					r.skipNow = false
				}
				fallthrough
			default:
				if !r.skipNow {
					n := utf8.EncodeRune(bytes, curR)
					r.buf = append(r.buf, bytes[0:n]...)
				}
			}
		}

		if e != nil {
			return e
		}
	}

	return err
}
